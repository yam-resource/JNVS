package com.control.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.control.util.HexUtil;

@Component
public class Gb28181Api {
	@Value("${gb28181.server-ip}")
	String gb28181Ip;
	@Value("${gb28181.server-http-port}")
	Integer gb28181HttpPort;
	@Autowired
	RestTemplate restTemplate;
	
	public String live(String deviceId,String channelId) {
		String result = restTemplate.getForObject("http://"+gb28181Ip+":"+gb28181HttpPort+"/gb28181/live/"+deviceId+"_"+channelId, String.class);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 200) {
			//System.out.println(jsonObj.getString("data"));
			//System.out.println("ssrc:"+HexUtil.intToHex(Integer.parseInt(jsonObj.getString("data"))));
			return HexUtil.intToHex(Integer.parseInt(jsonObj.getString("data")));
		} else {
			return null;
		}
	}
	public Boolean stopLive(String deviceId,String channelId) {
		String result = restTemplate.getForObject("http://"+gb28181Ip+":"+gb28181HttpPort+"/gb28181/bye/"+deviceId+"_"+channelId, String.class);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 200) {
			//System.out.println(jsonObj.getString("data"));
			//System.out.println("ssrc:"+HexUtil.intToHex(Integer.parseInt(jsonObj.getString("data"))));
			return true;
		} else {
			return false;
		}
	}
	public Boolean ptz(String deviceId,String channelId,int leftRight, int upDown, int inOut, int moveSpeed, int zoomSpeed) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("leftRight", leftRight);
		params.put("upDown", upDown);
		params.put("inOut", inOut);
		params.put("moveSpeed", moveSpeed);
		params.put("zoomSpeed", zoomSpeed);
		String result = restTemplate.getForObject("http://"+gb28181Ip+":"+gb28181HttpPort+"/gb28181/ptz/"+deviceId+"_"+channelId+"?leftRight={leftRight}&upDown={upDown}&inOut={inOut}&moveSpeed={moveSpeed}&zoomSpeed={zoomSpeed}", String.class,params);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 200) {
			return true;
		} else {
			return false;
		}
	}
}
