package com.control.db.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.control.db.dao.CameraRepository;
import com.control.db.entity.CameraEntity;
import com.control.db.service.CameraService;

@Service
public class CameraServiceImpl implements CameraService {
	@Autowired
	CameraRepository cameraRepository;

	@Override
	public List<CameraEntity> listAll(CameraEntity cameraEntity) {
		Example<CameraEntity> example = Example.of(cameraEntity);
		List<CameraEntity> cameraOptional = cameraRepository.findAll(example);
		return cameraOptional;
	}

	@Override
	public CameraEntity saveOrUpdate(CameraEntity cameraEntity) {
		// TODO Auto-generated method stub
		return cameraRepository.save(cameraEntity);
	}

	@Override
	public CameraEntity findOne(CameraEntity cameraEntity) {
		Example<CameraEntity> example = Example.of(cameraEntity);
		Optional<CameraEntity> cameraOptional = cameraRepository.findOne(example);
		if (cameraOptional.isPresent()) {
			CameraEntity cameraResult = cameraOptional.get();
			return cameraResult;
		} else {
			return null;
		}
	}

	@Override
	public void delete(CameraEntity cameraEntity) {
		cameraRepository.delete(cameraEntity);
	}

}
