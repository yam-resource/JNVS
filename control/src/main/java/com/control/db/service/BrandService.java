package com.control.db.service;

import java.util.List;

import com.control.db.entity.BrandEntity;

public interface BrandService {
	BrandEntity saveOrUpdate(BrandEntity brandEntity);
	BrandEntity findOne(BrandEntity brandEntity);
	List<BrandEntity> listAll(BrandEntity brandEntity);
}
