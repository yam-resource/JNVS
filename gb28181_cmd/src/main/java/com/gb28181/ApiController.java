package com.gb28181;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gb28181.bean.Device;
import com.gb28181.commons.ApiCodeEnum;
import com.gb28181.commons.ApiResult;

@RestController
@RequestMapping("/gb28181")
public class ApiController {
	@Autowired
	private SipLayer sipLayer;
	
	@GetMapping("/live/{deviceId}_{channelId}")
	public ApiResult live(@PathVariable String deviceId,@PathVariable String channelId){
		String ssrc = sipLayer.TransmitRequestInvite(deviceId, channelId);
		if(ssrc!=null) {
			return new ApiResult(ApiCodeEnum.OK,(Object)ssrc);
		} else {
			return new ApiResult(ApiCodeEnum.INVALID_REQUEST,(Object)ssrc);
		}
	}
	/***
	 * http://localhost:8080/api/ptz/34020000001320000002_34020000001320000008?leftRight=1&upDown=0&inOut=0&moveSpeed=50&zoomSpeed=0
	 * @param deviceId
	 * @param channelId
	 * @param leftRight
	 * @param upDown
	 * @param inOut
	 * @param moveSpeed
	 * @param zoomSpeed
	 * @return
	 */
	@GetMapping("/ptz/{deviceId}_{channelId}")
	public ApiResult ptz(@PathVariable String deviceId,@PathVariable String channelId,int leftRight, int upDown, int inOut, int moveSpeed, int zoomSpeed){
		sipLayer.TransmitRequestPtz(deviceId, channelId, leftRight, upDown, inOut, moveSpeed, zoomSpeed);;
		return new ApiResult(ApiCodeEnum.OK);
	}
	@GetMapping("/list")
	public ApiResult list(){
		List<Device> deviceList = new ArrayList<>();
		for (Map.Entry<String, Device> m : DeviceMap.deviceMap.entrySet()) {
			deviceList.add(m.getValue());
		}
		return new ApiResult(ApiCodeEnum.OK,deviceList);
	}
	/***
	 * 停止推流
	 * @return
	 */
	@GetMapping("/bye/{deviceId}_{channelId}")
	public ApiResult bye(@PathVariable String deviceId,@PathVariable String channelId) {
		sipLayer.TransmitRequestBye(deviceId, channelId);
		return new ApiResult(ApiCodeEnum.OK);
	}
}
