package com.gb28181.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gb28181.SipLayer;

@Configuration
public class SipLayerConfig {

	@Value("${sip.ip}")
	String sipIp;
	@Value("${sip.port}")
	Integer sipPort;
	@Value("${sip.device_id}")
	String sipDeviceId;
	@Value("${sip.domain}")
	String sipDomain;
	@Value("${sip.password}")
	String sipPassword;
	@Value("${media.ip}")
	String mediaIp;
	@Value("${media.rtp-port}")
	Integer mediaRtpPort;

	@Bean
	public SipLayer sipLayer(){
		SipLayer sipLayer = new SipLayer(sipIp, sipPort, sipDeviceId, sipDomain, sipPassword,mediaIp,mediaRtpPort);
		boolean startStatus = sipLayer.startServer();
		if(startStatus){
			System.out.println("Sip Server 启动成功 port {"+sipPort+"}");
		}else {
			System.out.println("Sip Server 启动失败");
		}
		return sipLayer;
	}
}
